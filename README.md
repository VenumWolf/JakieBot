# Jakie
Jakie is a general-purpose bot intended for use on the GFP Discord server.

**Table of Contents**
- [Jakie](#jakie)
- [Installation Instructions](#installation-instructions)
  - [Latest Stable Release](#latest-stable-release)
  - [Cloning the Project From Git](#cloning-the-project-from-git)
  - [Setting Up a Python Virtual Environment](#setting-up-a-python-virtual-environment)
  - [Installing Dependencies](#installing-dependencies)
  - [Running the Bot](#running-the-bot)
    - [Setting a Bot Token Though `config.py`](#setting-a-bot-token-though-configpy)
    - [Setting a Bot Token On Startup](#setting-a-bot-token-on-startup)
- [Commands / Cogs](#commands--cogs)
  - [Bot Utility Commands](#bot-utility-commands)
    - [Bot](#bot)
      - [Bug](#bug)
      - [Status](#status)
      - [Suggest](#suggest)
  - [Fun Commands](#fun-commands)
      - [Coinflip](#coinflip)
      - [Disemvowel](#disemvowel)
      - [Rps](#rps)
  - [General Utility Commands](#general-utility-commands)
      - [Getduration](#getduration)
      - [Log](#log)
    - [Info](#info)
      - [Member](#member)
      - [Server](#server)
  - [Meme Commands](#meme-commands)
    - [Fry](#fry)
      - [Light](#light)
      - [Medium](#medium)
      - [Deep](#deep)
      - [Burn](#burn)
      - [Nuke](#nuke)
      - [Pro](#pro)
  - [Moderation Commands](#moderation-commands)
      - [Ban](#ban)
      - [Cleanup](#cleanup)
      - [Cleanupfrom](#cleanupfrom)
      - [Kick](#kick)
  - [Other](#other)
      - [Help](#help)
- [Jakie's Todo List](#jakies-todo-list)

# Installation Instructions

## Latest Stable Release

The latest stable release can be found under the `stable` branch.  The stable branch is verified to be safe, and is the recommended release for production.

## Cloning the Project From Git

Ensure that you have git installed, then run the following in in your terminal:

```
git clone --single-branch --branch stable https://github.com/VenumWolf/JakieBot.git
```

This will clone the project onto your computer, a folder will be created in the current directory called: `JakieBot`.  You may change the name of the folder by adding it after the clone command above.

## Setting Up a Python Virtual Environment

If you do not have the venv module installed, you can install it with the following.

```
python -m pip install virtualenv
```

To create the virtual environment, run:

```
python -m venv env
```

This will create a virtual environment called `env`.

Next, activate the virtual environment (use the command for your given platform.)

**Linux / Bash**

```
source env/bin/activate
```

**Windows**

```
env\Scripts\activate.bat
```

When activated you should see `(env)` at the beginning of your command prompt.

## Installing Dependencies

First, ensure that your virtual environment is activated,and navigate to the project directory.

```
cd JakieBot
```

Then install the dependencies specified in `requirements.txt`.

```
pip install -r requirements.txt
```

## Running the Bot

Jakie's config is located in `jakie/config.py`, all relevant settings can be configured by editing this file.

### Setting a Bot Token Though `config.py`

Open the config in an editor of your choice, the first setting is `BOT_TOKEN`.  Copy and paste your token into the empty quotes.

```Python
BOT_TOKEN = "(your token here)"
```

### Setting a Bot Token On Startup

Bot tokens can be set via command arguments when starting up the program.

```
python bot.py --token (your token here)
```

# Commands / Cogs

Jakie contains no commands by default, as all commands are added via Discord.py's Commands Extension via Cogs.

**Note:** If you are running your own instance of the bot.  Mention it's name, rather than @Jakie.

## Bot Utility Commands

### Bot

A command group containing various bot-related utilities.

#### Bug
Submit a bug report to the bot owner.

Call the command with a description of the bug, as well as how you found it.  The report will be sent to the bot owner.

Usage: `@Jakie bot bug [bug_report : str]`

#### Status

Display the bot's current status.

Usage: `@Jakie bot status`

#### Suggest

Submit a suggestion to the bot owner.

Call the command with a suggestion and it will be sent to the bot owner.

## Fun Commands

#### Coinflip

Flips a coin, returning either heads or tails.

Usage: `@Jakie [coinflip|flip]`

#### Disemvowel

Remove all vowels from a text (not counting y as a vowel.)

Usage: `@Jakie disemvowel [text: str]`

#### Rps

Play a game of Rock Paper Scissors.

**How to play**

Call command followed by your pick of rock, paper, or scissors.

Game version 0.1.2

Usage: `@Jakie rps [choice: str=rock|paper|scissors]`

## General Utility Commands

#### Getduration

Convert seconds to a duration.

Usage: `@Jakie getduration [seconds : int]`

#### Log

Creates a log of the current channel in .log format.

The log file will be sent in a private message to whomever requests it.

User must have permission: *Manage Messages*

Usage: `@Jakie log [limit: int (default 100)]`


### Info

A command group containing various informational utilities.

#### Member

Displays information on a member.

Usage: `@Jakie info [member : discord.Member]`

#### Server

Displays information on the server.

Usage: `@Jakie info [server|guild]`

## Meme Commands

### Fry

Command group for cooking up some good 'ol deep fried memes.

Attachments should be in .jpg or .png format; some other formats can be accepted, but may produce unexpected results.

Calling fry on it's own will deep fry the meme.

NOTE: Linking to images does not work at the moment, though the feature is planned.


Usage: `@Jakie fry [image: discord.Attachment]`


#### Light

Makes 'em lightly crisped.

Usage: `@Jakie fry light [image: discord.Attachment]`

#### Medium

Good and fried, but not overdone.

Usage: `@Jakie fry medium [image: discord.Attachment]`

#### Deep

Now this is the stuff!

Usage: `@Jakie fry deep [image: discord.Attachment]`

#### Burn

Can memes be overcooked?

Usage: `@Jakie fry burn [image: discord.Attachment]`

#### Nuke

Hit the big red button...

Usage: `@Jakie fry nuke [image: discord.Attachment]`

#### Pro

If the presets just aren't good enough for you, the pro command allows you to do it yourself by taking your values, and plugging them into the deep fryer function.

**Parameters**

- contrast_factor: float
  
  - The contrast factor to use.

- color_factor: float

  - The color (saturation) factor to use.

- brightness_factor: float

  - The brightness factor to use.

- sharpness_factor : float

  - The contrast factor to use.

- jpeg_quality : int

  - The quality of the output jpeg.

- jpeg_repetitions : int

  - The number of times to resave the jpeg image (for that sweet crappy repost of crappy repost feel.)

Usage: `@Jakie fry pro [contrast_factor : float] [color_factor : float] [brightness_factor : float] [sharpness_factor : float] [jpeg_quality : int] [jpeg_repetitions : int] [image: discord.Attachment]`

## Moderation Commands

#### Ban

Ban one or more members by mentioning them. Members may be specified by @ mention, name, or ID.

Usage: `@Jakie ban [member: discord.Member] ...`

#### Cleanup

Clear a set number of messages.

**Restrictions**
- Cleanup can only delete up to 100 messages at a time (Discord imposed restriction.)
- Cleanup can only delete messages newer than 14 days (Discord imposed restriction.)

Usage: `@Jakie [cleanup|clear|del] [message_count: int (1 - 100)]`

#### Cleanupfrom

Clear messages from specific people.  Members may be specified by @ mention, name, or ID.

**Restrictions**
- Cleanupfrom can only delete up to 100 messages at a time (Discord imposed restriction.)
- Cleanupfrom can only delete messages newer than 14 days (Discord imposed restriction.)

Usage: `@Jakie [cleanupfrom|clearfrom|delfrom] [message_count: int (1 - 100)] [member: discord.Member] ...`

#### Kick

Kick one or more members by mentioning them. Members may be specified by @ mention, name, or ID.

Usage: `@Jakie ban [member: discord.Member] ...`

## Other

#### Help

Shows the help message.

Usage: `@Jakie help`

# Jakie's Todo List
- [ ] Prepare and release v0.5.
  - [x] Complete documentation.
    - [x] Add setup / installation instructions.
    - [x] Sync README documentation with command help documentation.
  - [ ] Draft release notes.