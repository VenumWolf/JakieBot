from helpers import create_dir_if_not_exist, get_prefix, format_duration
from discord.ext import commands
from helpers.get_prefix import get_prefix
from argparse import ArgumentParser
from datetime import datetime
import discord
import logging
import config
import sys
import os


class Bot(commands.AutoShardedBot):
    """
    The main bot class.

    Parameters
    ----------
    token : str
        The bot token used to log into discord.

    addons : list
        A list of addons to load on startup.
    """

    def __init__(self, token, addons, **kwargs):
        super().__init__(
            command_prefix=get_prefix(),
            activity=discord.Game(name=f'Loading...')
        )
        self.token = token
        self.addons = addons

        # Get log handlers.
        self.stream_log_handler = kwargs.get("stream_log_handler", logging.StreamHandler(stream=sys.stdout))
        self.file_log_handler = kwargs.get("file_log_handler", None)

        # Create bot logger.
        self.logger = logging.getLogger(config.BOT_LOGGER_NAME)

        # Register log handlers.
        self.logger.addHandler(self.stream_log_handler)
        if self.file_log_handler:
            self.logger.addHandler(self.file_log_handler)

        # Set log level.
        self.logger.setLevel(logging.INFO)

        # Track bot version:
        self.bot_version = "Development"

    def run(self):
        """
        Runs the bot.
        """
        # Load in all startup cogs.
        self.load_startup_cogs()

        # Attempt to connect.
        super().run(self.token, reconnect=True)

    async def shutdown(self):
        """
        Logs the bot out and stops the program.
        """
        self.logger.info("Starting shutdown procedures.")

        # Shutdown procedures here:
        await self.logout()

        # Add to log that shutdown has completed, exit the application.
        self.logger.info("Shutdown complete.  Quitting application.")
        sys.exit()

    def load_startup_cogs(self):
        """
        Loads startup cogs specified in config.py
        """
        for addon in self.addons:
            try:
                self.load_extension(addon)
            except Exception as e:
                self.logger.error(f"Failed to load cog'{addon}'\n\t{e}")
                next
            else:
                self.logger.info(f"Loaded extension'{addon}'")

    async def on_ready(self):
        """
        Indicate when the bot is fully operational.
        """
        await self.change_presence(activity=discord.Game(name=f'@{self.user} help'))

        if not hasattr(self, "start_time"):
            self.start_time = datetime.now()
            self.logger.info("Startup complete.")

        else:
            self.logger.info("Resuming operation.")

        # Indicate that the bot is ready for action.
        self.logger.info(f"Ready to process events as as {self.user}")

    async def on_command_error(self, ctx, error):
        """
        Global command error handling.  Catches all errors raised by commands.
        """
        if isinstance(error, commands.MissingRequiredArgument):
            # Private message the user to let them know they are missing something.
            if ctx.command.usage:
                await ctx.author.send(
                    f"Missing required argument `{error.param}`.\n"
                    f"You can type `@{self.user} help {ctx.command.qualified_name}` for instructions"
                    f" and usage information."
                )

        elif isinstance(error, commands.BadArgument):
            # If the command has it, private message the command usage information to the user.
            if ctx.command.usage:
                await ctx.author.send(f"Usage: `@{self.user} {ctx.command.qualified_name} {ctx.command.usage}`")

        elif isinstance(error, commands.CommandNotFound):
            # Log error as debug message.
            self.logger.debug(f"{error}")

        elif isinstance(error, commands.CommandOnCooldown):
            # Private message the user to let them know when they may use the command again.
            await ctx.author.send(f"The command is on cooldown, you may use it again in "
                                  "{format_duration(error.retry_after)}.")
        elif isinstance(error, commands.NoPrivateMessage):
            # Private message the user to let them know they must run the command in a server.
            await ctx.author.send("The command can only be used in a server.")

        elif isinstance(error, commands.DisabledCommand):
            # Private message the user to let them know the command is disabled.
            await ctx.author.send("The command has been disabled.  It cannot be run at this time.")

        else:
            self.logger.error(f"[{ctx.command}]: {error}")

    async def on_message(self, message):
        await self.process_commands(message)


if __name__ == "__main__":
    # Ensure that the bot's working directory exists.
    create_dir_if_not_exist(config.WORKING_DIR)

    # Set up global logging (for discord.)
    logger = logging.getLogger('discord')
    logger.setLevel(logging.DEBUG)

    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(logging.Formatter(config.LOGGER_FORMAT))
    stream_handler.setLevel(logging.INFO)
    logger.addHandler(stream_handler)

    file_handler = logging.FileHandler(os.path.join(config.WORKING_DIR, "log_latest.log"), encoding='utf-8', mode='w')
    file_handler.setFormatter(logging.Formatter(config.LOGGER_FORMAT))
    logger.addHandler(file_handler)

    # Handle command line arguments.
    argument_parser = ArgumentParser()
    argument_parser.add_argument("--token", "-t", default=None, type=str, help="The Discord API token to connect with.")
    arguments = argument_parser.parse_args()

    # Get bot token.
    if arguments.token:
        bot_token = arguments.token
    else:
        bot_token = config.BOT_TOKEN
        
    # Run the bot.
    Bot(
        bot_token,
        config.EXTENSIONS,
        stream_log_handler=stream_handler,
        file_log_handler=file_handler
    ).run()
