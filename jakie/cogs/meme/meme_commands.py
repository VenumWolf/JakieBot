from .attachment_is_image import attachment_is_image
from .fry_command import fry_command
from discord.ext import commands
import discord

class MemeCommands(commands.Cog, name="Meme Commands"):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(
        name="fry",
        brief="Cook up some good 'ol deepfried memes.",
        description=
        "Command group for cooking up some good 'ol deep fried memes.\n"
        "Calling `fry` on it's own will deepfry the meme.\n\n"
        "NOTE: Linking to images does not work at the moment, though the feature is planned.",
        usage="[image: discord.Attachment]"
    )
    async def fry_command_group(self, ctx):
        if ctx.invoked_subcommand is None:
            await fry_command(
                ctx=ctx,
                completion_message=f"Your meme has been deepfried, {ctx.author.mention}.",
                contrast_factor=10,
                color_factor=4.8,
                brightness_factor=0.8,
                sharpness_factor=60,
                jpeg_quality=5
            )

    @fry_command_group.command(
        name="light",
        brief="Makes 'em lightly crisped.",
        usage="[image: discord.Attachment]"
    )
    async def fry_light_command(self, ctx):
        await fry_command(
            ctx=ctx,
            completion_message=f"Your meme, lightly crisped {ctx.author.mention}.",
            contrast_factor=3.0,
            color_factor=2.0,
            brightness_factor=1.2,
            sharpness_factor=10,
            jpeg_quality=15
        )

    @fry_command_group.command(
        name="medium",
        brief="Good and fried, but not overdone.",
        usage="[image: discord.Attachment]"
    )
    async def fry_medium_command(self, ctx):
        await fry_command(
            ctx=ctx,
            completion_message=f"Like 'em medium rare, {ctx.author.mention}?",
            contrast_factor=6.4,
            color_factor=4.0,
            brightness_factor=1.2,
            sharpness_factor=20,
            jpeg_quality=10
        )

    @fry_command_group.command(
        name="deep",
        brief="Now this is the stuff.",
        usage="[image: discord.Attachment]"
    )
    async def fry_deep_command(self, ctx):
        await fry_command(
            ctx=ctx,
            completion_message=f"Your meme has been deepfried, {ctx.author.mention}.",
            contrast_factor=10,
            color_factor=4.8,
            brightness_factor=0.8,
            sharpness_factor=60,
            jpeg_quality=5
        )

    @fry_command_group.command(
        name="burn",
        brief="Can memes be overcooked?",
        usage="[image: discord.Attachment]"
    )
    async def fry_burn_command(self, ctx):
        await fry_command(
            ctx=ctx,
            completion_message=f"I have burned the meme for you, {ctx.author.mention}",
            contrast_factor=15,
            color_factor=6.0,
            brightness_factor=0.7,
            sharpness_factor=60,
            jpeg_quality=3
        )

    @fry_command_group.command(
        name="nuke",
        brief="Hit the big red button...",
        usage="[image: discord.Attachment]"
    )
    async def fry_nuke_command(self, ctx):
        await fry_command(
            ctx=ctx,
            completion_message=f"There's nothing left, {ctx.author.mention}.",
            contrast_factor=20,
            color_factor=7.0,
            brightness_factor=0.7,
            sharpness_factor=50,
            jpeg_quality=1
        )

    @fry_command_group.command(
        name="pro",
        brief="Fry it your way, if you know what you're doing.",
        description="If the presets just aren't good enough for you, the pro fryer command allows you to do it "
                    "yourself by taking your values, and plugging them into the deep fryer function.\n"
                    "**Parameters**\n"
                    "contrast_factor: float\n"
                    "\tThe contrast factor to use.\n"
                    "color_factor: float\n"
                    "\tThe color (saturation) factor to use.\n"
                    "brightness_factor: float\n"
                    "\tThe brightness factor to use.\n"
                    "sharpness_factor : float\n"
                    "\tThe contrast factor to use.\n"
                    "jpeg_quality : int\n"
                    "\tThe quality of the output jpeg.\n"
                    "jpeg_repetitions : int\n"
                    "\tThe number of times to resave the jpeg image"
                    "(for that sweet crappy repost of crappy repost feel.)\n",
        usage="[contrast_factor : float] [color_factor : float] [brightness_factor : float] [sharpness_factor : float]"
              "[jpeg_quality : int] [jpeg_repetitions : int] [image: discord.Attachment]"
    )
    async def fry_pro_command(self, ctx, contrast_factor: float, color_factor: float, brightness_factor: float,
                              sharpness_factor: float, jpeg_quality: int, jpeg_repetitions: int):
        await fry_command(
            ctx=ctx,
            completion_message=f"{ctx.author.mention}\nParameters: {contrast_factor} {color_factor}"
                               f"{brightness_factor} {sharpness_factor} {jpeg_quality} {jpeg_repetitions}",
            contrast_factor=contrast_factor,
            color_factor=color_factor,
            brightness_factor=brightness_factor,
            sharpness_factor=sharpness_factor,
            jpeg_quality=jpeg_quality,
            jpeg_repetitions=jpeg_repetitions
        )
