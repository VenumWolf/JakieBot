def attachment_is_image(attachment):
    """
    Ensures that a discord attachment is an image.
    """
    return attachment.height and attachment.width 