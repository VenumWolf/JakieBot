from helpers.clamp import clamp
from PIL import Image, ImageEnhance
from io import BytesIO


def deepfry(image, **kwargs):
    """
    Deep fry an image.

    Parameters
    ----------
    image : BytesIO
        A file-like object containing of the input image data.
    Kwargs
    ------
    contrast_factor: float
        The contrast factor to use. 1.0 for original image value.
        
    color_factor: float
        The color (saturation) factor to use. 1.0 for original image value.
    
    brightness_factor: float
        The brightness factor to use. 1.0 for original image value.

    sharpness_factor : float
        The contrast factor to use. 1.0 for original image value.

    jpeg_quality : int
        The quality of the output jpeg.

    jpeg_repetitions : int
        The number of times to resave the jpeg image (for that sweet crappy repost of crappy repost feel.)

    Returns
    -------
    bytes :
        Binary data for deep-fried image in .jpg format.
    """
    # Handle kwargs.
    contrast_factor = float(kwargs.get("contrast_factor", 1.0))
    color_factor = float(kwargs.get("color_factor", 1.0))
    brightness_factor = float(kwargs.get("brightness_factor", 1.0))
    sharpness_factor = float(kwargs.get("sharpness_factor", 1.0))
    jpeg_quality = int(kwargs.get("jpeg_quality", 10))
    jpeg_repetitions = int(kwargs.get("jpeg_repetitions", 1))

    # Create PIL image.
    fried_image = Image.open(image).convert("RGB")
    
    # Apply contrast adjustments.
    contrast = ImageEnhance.Contrast(fried_image)
    fried_image = contrast.enhance(contrast_factor)

    # Apply color / saturation adjustments.
    color = ImageEnhance.Color(fried_image)
    fried_image = color.enhance(color_factor)

    # Apply brightness adjustments.
    brightness = ImageEnhance.Brightness(fried_image)
    fried_image = brightness.enhance(brightness_factor)

    # Apply sharpness adjustments.
    sharpness = ImageEnhance.Sharpness(fried_image)
    fried_image = sharpness.enhance(sharpness_factor)

    # Jpeg repetitions are too heavy not to impose restrictions on.
    jpeg_repetitions = clamp(jpeg_repetitions, 1, 100)

    # Write final file object.
    fried_image_file = BytesIO()
    for i in range(jpeg_repetitions):
        fried_image.save(fried_image_file, "JPEG", quality=jpeg_quality)
        fried_image = Image.open(fried_image_file).convert("RGB")

    return fried_image_file.getvalue()