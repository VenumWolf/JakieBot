from .attachment_is_image import attachment_is_image
from discord.ext import commands
from .deep_fryer import deepfry
from io import BytesIO
import discord


async def fry_command(ctx, completion_message=None, contrast_factor=1.0, color_factor=1.0, brightness_factor=1.0,
                      sharpness_factor=1.0, jpeg_quality=100, jpeg_repetitions=1):
    """
    The main logic behind the fry subcomands.

    Parameters
    ----------
    ctx:
        The command context.  Automatically passed by Discord.py.
    completion_message : str
        The message to send with the image when the command has completed.
    contrast_factor: float
        The contrast factor to use. 1.0 for original image value.
        
    color_factor: float
        The color (saturation) factor to use. 1.0 for original image value.
    
    brightness_factor: float
        The brightness factor to use. 1.0 for original image value.

    sharpness_factor : float
        The contrast factor to use. 1.0 for original image value.
    
    jpeg_quality : int
        The quality of the output jpeg.

    jpeg_repetitions: int
        The number of times to rewrite the image (for that sweet repost of a repost feeling.)
    """
    if len(ctx.message.attachments):
        for attachment in ctx.message.attachments:
            if attachment_is_image(attachment):
                async with ctx.typing():
                    try:
                        image = BytesIO(b"")
                        await attachment.save(image)
                        if not isinstance(ctx.channel, discord.DMChannel):
                            await ctx.message.delete()
                        image = deepfry(
                            image=image,
                            contrast_factor=contrast_factor,
                            color_factor=color_factor,
                            brightness_factor=brightness_factor,
                            sharpness_factor=sharpness_factor,
                            jpeg_quality=jpeg_quality,
                            jpeg_repetitions=jpeg_repetitions
                        )
                        await ctx.send(
                            completion_message,
                            file=discord.File(image, filename="fried.jpg")
                        )
                    except Exception as e:
                        raise e
    else:
        raise commands.BadArgument()
