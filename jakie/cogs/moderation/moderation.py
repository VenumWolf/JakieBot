# External Libraries
from discord.ext import commands
import discord
import asyncio

class Moderation(commands.Cog, name="Moderation Commands"):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name="ban",
        brief="Ban members by mention.",
        usage="[member: discord.Member] ...",
        description="Ban one or more members by mentioning them."
                    "Members may be specified by @ mention, name, or ID.\n\n"
                    "**Required Permissions**\n"
                    "\t- *Ban Members*\n\n",
    )
    @commands.guild_only()
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, *members: discord.Member):
        async with ctx.typing():
            if len(members):
                ban_message = "I could not issue any bans."
                successful_bans = []
                for member in members:
                    try:
                        await member.ban(delete_message_days=7)
                        successful_bans.append(member)
                    except discord.Forbidden as e:
                        await ctx.send("You do not have the required permissions to use this command.")
                        break
                    except discord.HTTPEXception:
                        await ctx.send(f"Failed to ban {member}")
                if len(successful_bans):
                    ban_message = (
                        "They're gone, the won't be bothering us again!\n"
                        f'Banned {len(successful_bans)}:\n'
                    )
                    for member in successful_bans:
                        ban_message += f'\t{member.display_name}\n'
                await ctx.send(ban_message)

    @commands.command(
        name="kick",
        brief="Kick members.",
        usage="@member1 @member2 ...",
        description="Kick one or more members by mentioning them."
                    "Members may be specified by @ mention, name, or ID.\n\n"
                    "**Required Permissions**\n"
                    "\t- *Kick Members*\n\n",
    )
    @commands.guild_only()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, *members: discord.Member):
        async with ctx.typing():
            if len(members):
                kick_message = "I could not kick anyone."
                successful_kicks = []
                for member in members:
                    try:
                        await member.kick()
                        successful_kicks.append(member)
                    except discord.Forbidden as e:
                        await ctx.send("You do not have the required permissions to use this command.")
                        break
                    except discord.HTTPEXception:
                        await ctx.send(f"Failed to kick {member}")
                if len(successful_kicks):
                    kick_message = (
                        "They're gone, but they may return.  Stay sharp!\n"
                        f'Kicked {len(successful_kicks)}:\n'
                    )
                    for member in successful_kicks:
                        kick_message += f'\t{member.display_name}\n'
                await ctx.send(kick_message)

    @commands.command(
        name="cleanup",
        aliases=["clear", "del"],
        brief="Clear a set number of messages.",
        usage="[number: message count (1 - 100)]",
        description="Clear a set number of messages.\n\n"
                    "**Required Permissions**\n"
                    "\t- *Manage Messages*\n\n"
                    "**Restrictions**\n"
                    "\t- Cleanup can only delete up to 100 messages at a time (Discord imposed restriction.)\n"
                    "\t- Cleanup can only delete messages newer than 14 days (Discord imposed restriction.)"
    )
    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    async def cleanup_messages(self, ctx, message_count: int, **kwargs):
        await self.cleanup_logic(ctx, await ctx.channel.history(limit=message_count).flatten())

    @commands.command(
        name="cleanupfrom",
        aliases=["clearfrom", "delfrom"],
        brief="Cleanup messages from specific people.",
        usage="[number: message count(1 - 100)] [mention: member1] [mention: member2] ...",
        description="Clear messages from specific people."
                    "**Required Permissions**\n"
                    "\t- *Manage Messages*\n\n"
                    "**Restrictions**\n"
                    "\t- Cleanupfrom can only delete up to 100 messages at a time (Discord imposed restriction.)\n"
                    "\t- Cleanupfrom can only delete messages newer than 14 days (Discord imposed restriction.)"
    )
    @commands.has_permissions(manage_messages=True)
    async def cleanup_messages_from(self, ctx, message_count: int, *members: discord.Member):
        def from_filter(message):
            return message.author in members

        await self.cleanup_logic(ctx, await ctx.channel.history(limit=message_count).filter(from_filter).flatten())

    async def cleanup_logic(self, ctx, messages):
        """
        Business logic for the cleanup commands.
        """
        async with ctx.typing():
            # Delete invocation message.
            await ctx.message.delete()

            # Generate message strings.
            message_length_str = f"{str(len(messages)) + ' message' if len(messages) else ' messages'}"
            if len(messages):
                final_message = f"Successfully deleted {message_length_str}."
            else:
                final_message = f"No messages could be deleted."
            # Attempt to delete the messages.
            try:
                await ctx.channel.delete_messages(messages)
            except discord.ClientException:
                final_message = "You may not delete more than 100 messages at a time " \
                    f"(you tried to delete {message_length_str}.)"
            except discord.Forbidden:
                final_message = "I do not have the required permissions to delete messages, please contact an admin."
            except discord.HTTPException as e:
                final_message = f"Encountered issue: {e}"
                self.bot.logger.error(f"[{ctx.command}]: {e}")
            except Exception as e:
                raise e
            finally:
                await ctx.send(final_message, delete_after=5)
