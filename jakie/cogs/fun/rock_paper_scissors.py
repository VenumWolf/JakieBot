from random import choice


class RockPaperScissorsGame:
    """
    Rock Paper Scissors game for Jakie.
    Version: 0.1.2
    """

    def __init__(self, player_choice):
        self.plays = ('rock', 'paper', 'scissors')
        self.play_emojis = {
            'rock': ":full_moon:",
            'paper': ':newspaper:',
            'scissors': ':scissors:'
        }
        self.computer_choice = choice(self.plays)
        self.player_choice = player_choice.lower()
        if not self.is_player_choice_valid():
            raise ValueError()

    def is_player_choice_valid(self):
        return self.player_choice in self.plays

    def get_winner(self):
        if self.computer_choice == self.player_choice:
            return 'draw'
        elif (self.computer_choice == 'rock' and self.player_choice == 'paper') or (
                self.computer_choice == 'paper' and self.player_choice == 'scissors') or (
                self.computer_choice == 'scissors' and self.player_choice == 'rock'):
            return 'player'
        elif (self.computer_choice == 'paper' and self.player_choice == 'rock') or (
                self.computer_choice == 'scissors' and self.player_choice == 'paper') or (
                self.computer_choice == 'rock' and self.player_choice == 'scissors'):
            return 'computer'

    def get_play_emoji(self, play):
        return self.play_emojis.get(play)
