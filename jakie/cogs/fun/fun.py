from discord.ext import commands
from .rock_paper_scissors import RockPaperScissorsGame
import discord

import asyncio
from random import choice

class FunCommands(commands.Cog, name="Fun Commands"):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name="disemvowel",
        brief="Remove vowels from a text.",
        usage="[text: str]",
        description="Remove all vowels from a text (not counting y as a vowel.)"
    )
    async def disemvowel_string(self, ctx, *, text):
        await ctx.send("".join([char for char in text if char.lower() not in 'aeiou']))

    @commands.command(
        name="coinflip",
        aliases=["flip"],
        brief="Flips a coin.",
        usage="",
        description="Flips a coin, returning either heads or tails."
    )
    async def coin_flip(self, ctx):
        await ctx.send(choice(["Heads", "Tails"]))

    @commands.command(
        name="rps",
        brief="Play a game of Rock Paper Scissors.",
        usage="[choice: str rock|paper|scissors]",
        description="Play a game of Rock Paper Scissors\n\n"
                    "**How to play**\n"
                    "Call command followed by your pick of `rock`, `paper`, or `scissors`.\n\n"
                    "Game version 0.1.2"
    )
    async def rock_paper_scissors(self, ctx, choice: str):
        try:
            game = RockPaperScissorsGame(choice)
            embed = discord.Embed(title="Rock Paper Scissors",
                                  description=f'{ctx.message.author.name} vs. {self.bot.user}')
            embed.add_field(name=f'{self.bot.user.name}\'s choice',
                            value=f'{game.get_play_emoji(game.computer_choice)}')
            embed.add_field(name=f'{ctx.message.author.name}\'s choice',
                            value=f'{game.get_play_emoji(game.player_choice)}')
            embed.add_field(name="Winner", value='Draw' if game.get_winner() == 'draw' else (
                ctx.message.author.name if game.get_winner() == 'player' else self.bot.user.name))
            await ctx.send(embed=embed)
        except ValueError:
            # Raise bad argument and allow global handler to do it's job.
            raise commands.BadArgument()
