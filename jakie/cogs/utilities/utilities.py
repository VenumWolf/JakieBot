from helpers.format_duration import InvalidDurationException, format_duration
from discord.ext import commands
from datetime import datetime
import discord
import asyncio


class Utilities(commands.Cog, name="General Utility Commands"):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name="log",
        brief="Creates a log of the current channel in .log format.",
        usage="[number: limit (default=100)]",
        description="Creates a log of the current channel in .log format\n"
                    "The log file will be sent in a private message to whomever requests it.\n\n"
                    "**Parameters**\n"
                    "\tlimit: The number of messages to save."
                    "**Required Permissions**\n"
                    "\t*Manage Messages*\n\n"
    )
    @commands.has_permissions(manage_messages=True)
    @commands.guild_only()
    async def channel_log(self, ctx, limit=100):
        # Initialize variables.
        date_format = "%d:%m:%y:%H:%M"
        final_status_emoji = "✅"

        # Indicate that something is happening.
        await ctx.message.add_reaction("🆗")

        try:
            # Send log file to command sender via private message.
            await ctx.author.send(
                file=discord.File(
                    "\n".join(
                        reversed(
                            [
                                f"[{message.created_at.strftime(date_format)} {message.author}]: {message.content}"
                                async for message in ctx.channel.history(limit=limit)
                            ])).encode(encoding="UTF-8"),
                    filename=f"{ctx.message.guild.name.lower()}_{ctx.message.channel.name.lower()}"
                    f"{datetime.now().strftime(date_format)}.log"
                )
            )
            # logger.info(f"Sent log file to: {ctx.message.author}")

        except Exception as e:
            # logger.warn(f"Could not send log to {ctx.message.author}: {e}")
            await ctx.author.send("Seems that something has gone wrong when trying to create the logfile.")
            # To indicate something went wrong.
            final_status_emoji = "🚫"

        finally:
            # Update indication to show that the process has completed.
            await ctx.message.remove_reaction("🆗", self.bot.user)
            await ctx.message.add_reaction(final_status_emoji)

            # Wait 5 seconds then remove the log command.
            await asyncio.sleep(5)
            await ctx.message.delete()

    @commands.group(
        name="info",
        aliases=["about"],
        usage="",
        brief="Command group of various informational utilities."
    )
    async def info(self, ctx):
        pass

    @info.command(
        name="member",
        brief="Displays information on a member.",
        usage="[member: discord.Member]",
        description="Displays information on a member."
    )
    @commands.guild_only()
    async def info_member(self, ctx, member: discord.Member):
        embed = discord.Embed(
            title=f'About {member.display_name}',
            description=f'Status: {member.status}',
        )
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="'Real' name", value=member)
        embed.add_field(name="Joined server on", value=member.joined_at.strftime("%m/%d/%Y"))
        embed.add_field(name="Highest role", value=member.top_role)
        await ctx.send(embed=embed)

    @info.command(
        name="server",
        aliases=["guild"],
        brief="Displays information on the server.",
        usage="",
        description="Displays information about the server."
    )
    @commands.guild_only()
    async def info_server(self, ctx):
        embed = discord.Embed(
            title=f'About {ctx.guild.name}'
        )
        embed.set_thumbnail(url=ctx.guild.icon_url)
        embed.add_field(name="Owner", value=ctx.guild.owner)
        embed.add_field(name="Members", value=ctx.guild.member_count)
        embed.add_field(name="Creation date", value=ctx.guild.created_at.strftime("%m/%d/%Y"))
        await ctx.send(embed=embed)

    @commands.command(
        name="getduration",
        brief="Convert seconds to a duration.",
        usage="[seconds : int]",
        description="Convert seconds to a duration."
    )
    async def get_duration_command(self, ctx, seconds: int):
        try:
            await ctx.send(format_duration(seconds))
        except InvalidDurationException:
            raise (commands.BadArgument(message="Duration must be a positive number."))
