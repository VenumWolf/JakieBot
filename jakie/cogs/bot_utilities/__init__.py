from .bot_commands import BotCommands


def setup(bot):
    bot.add_cog(BotCommands(bot))
