import discord
from datetime import datetime, timedelta
from helpers.format_duration import format_duration


def get_bot_status_embed(ctx, bot):
    """
    Creates the bot status / info embed.

    Parameters
    ----------
    ctx : discord.Context
        The current command context.

    bot : discord.Bot
        Reference to the bot.

    Returns
    -------
    discord.Embed:
        The discord embed object.
    """
    addons = "\n".join(bot.extensions)
    uptime = datetime.now() - bot.start_time
    embed = discord.Embed(
        title=f'Status',
        url="https://github.com/VenumWolf/JakieBot"
    )
    embed.add_field(name="Version", value=bot.bot_version)
    embed.add_field(name="Latency", value=f'{int(bot.latency * 1000)}ms', inline=False)
    embed.add_field(name="Connected Servers", value=f'{len(bot.guilds)}', inline=False)
    embed.add_field(name="Uptime", value=format_duration(uptime.total_seconds()), inline=False)
    embed.add_field(name="Addons Loaded", value=f'{addons}', inline=False)
    return embed
