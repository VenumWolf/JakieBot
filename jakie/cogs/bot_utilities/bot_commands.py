from .get_bot_status_embed import get_bot_status_embed
from discord.ext import commands
import discord


class BotCommands(commands.Cog, name="Bot Utility Commands"):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(
        name="bot",
        brief="Command group with various bot-related utilities.",
        alieases=["botutils", "botutilities"]
    )
    async def utilities(self, ctx):
        pass

    @utilities.command(
        name="status",
        brief="Displays the bot's current status.",
        usage="",
        description="Displays the bot's current status."
    )
    async def info_bot(self, ctx):
        app_info = await self.bot.application_info()
        embed = get_bot_status_embed(ctx, self.bot)
        embed.title = f'About {self.bot.user}'
        embed.description = app_info.description
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        await ctx.send(embed=embed)

    @utilities.command(
        name="stop",
        brief="(Owner only) Forcefully stop the bot.",
        description="(Owner only)\nForcefully stop the bot.",
        hidden=True
    )
    @commands.is_owner()
    async def stop(self, ctx):
        await ctx.author.send("Shutting down.")
        self.bot.shutdown()

    @utilities.command(
        name="load",
        brief="(Owner only) Loads an extension.",
        description="(Owner only)\nLoads an extension.",
        usage="[string: module]",
        hidden=True
    )
    @commands.is_owner()
    async def load_module(self, ctx, module):
        if module not in self.bot.extensions:
            try:
                self.bot.load_extension(module)
            except Exception as e:
                await ctx.send(f'```Failed to load module {module}:\n{e}```')
            else:
                await ctx.send(f'Loaded module: `{module}`')
        else:
            await ctx.send(f'Module `{module}` is already loaded.')

    @utilities.command(
        name="unload",
        brief="(Owner only) Unloads an extension.",
        description="(Owner only)\nUnloads an extension",
        usage="[string: module]",
        hidden=True
    )
    @commands.is_owner()
    async def unload_module(self, ctx, module):
        """Unloads a module."""
        if module in self.bot.extensions:
            try:
                self.bot.unload_extension(module)
            except Exception as e:
                await ctx.send(f'```Failed to unload module {module}:\n{e}```')
            else:
                await ctx.send(f'Unloaded module: `{module}`')
        else:
            await ctx.send(f'Module `{module}` not found.')

    @utilities.command(
        name="reload",
        brief="(Owner only) Reloads an extension.",
        description="(Owner only)\nReloads an extension.",
        usage="[string: module]",
        hidden=True
    )
    @commands.is_owner()
    async def reload_module(self, ctx, module):
        """Reloads a module."""
        if module in self.bot.extensions:
            try:
                self.bot.reload_extension(module)
            except Exception as e:
                await ctx.send(f'```Failed to reload module {module}:\n{e}```')
            else:
                await ctx.send(f'Reloaded module: `{module}`')
        else:
            await ctx.send(f'Module `{module}` not found.')
