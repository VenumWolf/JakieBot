import os

BOT_TOKEN = ""

# Data file config.
WORKING_DIR = os.path.join(os.getcwd(), "bot_data")

# Logging config.
BOT_LOGGER_NAME = "Jakie"
LOGGER_FORMAT = "[%(asctime)s %(levelname)s]: [%(name)s] %(message)s"

# Extensions.
EXTENSIONS = [
    'cogs.bot_utilities',
    'cogs.utilities',
    'cogs.moderation',
    'cogs.fun',
    'cogs.meme',
]
