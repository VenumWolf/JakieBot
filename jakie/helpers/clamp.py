def clamp(value, minimum, maximum):
    return minimum if value < minimum else maximum if value > maximum else value