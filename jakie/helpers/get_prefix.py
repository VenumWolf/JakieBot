from discord.ext.commands import when_mentioned_or


def get_prefix():
    return when_mentioned_or(".gfp")
