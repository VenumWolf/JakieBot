import os


def create_dir_if_not_exist(dir):
    """
    Creates the folder if it does not already exist.

    Parameters
    ----------
    dir : str
        The directory to create.
    """
    if not os.path.exists(dir):
        os.mkdir(dir)
