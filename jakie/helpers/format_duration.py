class InvalidDurationException(Exception):
    pass


def format_duration(seconds):
    """
    Converts a number of seconds a more human readable format.

    Parameters
    ----------
    seconds : int
        The number of seconds to convert.

    Returns
    -------
    str :
        The formatted duration.
    """
    if seconds < 0:
        raise InvalidDurationException("Duration cannot be a negative number.")
    years, seconds = divmod(seconds, 31557600)
    months, seconds = divmod(seconds, 2592000)
    weeks, seconds = divmod(seconds, 604800)
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    return (
        f"{str(int(years)) + 'y ' if years != 0 else ''}"
        f"{str(int(months)) + 'm ' if months != 0 else ''}"
        f"{str(int(weeks)) + 'w ' if weeks != 0 else ''}"
        f"{str(int(days)) + 'd ' if days != 0 else ''}"
        f"{str(int(hours)) + 'h ' if hours != 0 else ''}"
        f"{str(int(minutes)) + 'm ' if minutes != 0 else ''}"
        f"{str(int(seconds)) + 's' if seconds != 0 or years + months + weeks + days + hours + minutes == 0 else ''}"
    )
