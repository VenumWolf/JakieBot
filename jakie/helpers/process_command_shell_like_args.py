from shlex import split


def process_command_shell_like_args(arg_string):
    """
    Takes in a shell-like argument string and returns
    a dictionary of command name and values.

    Parameters
    ----------
    args_string : str
        The raw string arguments.
    """
    # Initialize variables.
    arg_dict = dict()
    args = split(arg_string)

    # Create dictionary from args.
    for i in range(0, len(args) - 1, 2):
        # Add key value pair to arg_dict.
        arg_dict.update({args[i].strip("--"): args[i + 1]})

    return arg_dict
